package com.indiant.process;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Transaction;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.indiant.beans.Pais;
import com.indiant.dao.PMF;
import com.indiant.logic.LogicPais;
import com.indiant.shared.BeanParametro;

public class GestionMantenimiento {
	
	public static boolean insertarPais(Pais bean) throws Exception{		
		PersistenceManager pm=null;
		Transaction tx=null;
		try{
			pm=PMF.getPMF().getPersistenceManager();
			tx=pm.currentTransaction();
			tx.begin();
			LogicPais logic=new LogicPais(pm);
			BeanParametro beanParametro=new BeanParametro();
			Key keyPais=KeyFactory.createKey(Pais.class.getSimpleName(), bean.getIdPais());
			String idPais=KeyFactory.keyToString(keyPais);
			bean.setIdPais(idPais);
			beanParametro.setBean(bean);
			beanParametro.setTipoOperacion("I");
			logic.mantenimiento(beanParametro);
			tx.commit();
			pm.close();
			return true;	
		}catch(Exception ex){
			throw ex;
		}finally{
			if(!pm.isClosed()){
				if(tx.isActive()){
					tx.rollback();
				}
				pm.close();
			}
		}
	}
	
	public static List<Pais> getListPais() throws Exception{
		PersistenceManager pm=null;
		try{
			pm=PMF.getPMF().getPersistenceManager();
			LogicPais logic=new LogicPais(pm);
			return logic.getListBean();
		}catch(Exception ex){
			throw ex;
		}finally{
			if(!pm.isClosed()){
				pm.close();
			}
		}
	}
}
