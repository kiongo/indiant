package com.indiant.logic;

import java.util.List;

import javax.jdo.PersistenceManager;

import com.indiant.beans.Pais;
import com.indiant.dao.DaoPais;
import com.indiant.shared.BeanParametro;

public class LogicPais {
private PersistenceManager pm;
	
	public LogicPais(PersistenceManager pm){
		this.pm=pm;
	}
	
	public boolean mantenimiento(BeanParametro parametro) throws Exception{
		DaoPais dao=new DaoPais(pm);
		return dao.mantenimiento(parametro);
	}
	
	public List<Pais> getListBean()throws Exception{
		DaoPais dao=new DaoPais(pm);
		return dao.getListBean();
	}
}
