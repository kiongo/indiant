package com.indiant.dao;

import java.util.List;

import javax.jdo.PersistenceManager;

import com.indiant.beans.Pais;
import com.indiant.shared.BeanParametro;

public class DaoPais {
	private PersistenceManager pm;
	
	public DaoPais(PersistenceManager pm){
		this.pm=pm;
	}
	
	public boolean mantenimiento(BeanParametro parametro) throws Exception{
		Querys query=new Querys(pm);
		return query.mantenimiento(parametro);
	}
	
	public List<Pais> getListBean() throws Exception{
		Querys query=new Querys(pm);
		return (List<Pais>)query.getListBean(Pais.class);
	}
}
