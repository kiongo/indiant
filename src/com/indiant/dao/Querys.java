package com.indiant.dao;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.indiant.shared.BeanParametro;

public class Querys {
	private PersistenceManager pm;
	
	public Querys(){
		
	}
	
	public Querys(PersistenceManager pm){
		this.pm=pm;
	}
	
	public boolean mantenimiento(BeanParametro parametro) throws Exception{
		try{
			if(parametro.getTipoOperacion().equalsIgnoreCase("I") || 
					parametro.getTipoOperacion().equalsIgnoreCase("A")){
				pm.makePersistent(parametro.getBean());
				return true;
			}else if(parametro.getTipoOperacion().equalsIgnoreCase("E")){
				Object bean=pm.getObjectById(parametro.getBean().getClass(),parametro.getId());
				pm.deletePersistent(bean);
				return true;
			}else{
				throw new Exception("No se ha definido operacion");
			}
		}catch(Exception ex){
			throw ex;
		}
	}
	
	public List<?> getListBean(Class<?> nomClass)throws Exception{
		Query query=pm.newQuery(nomClass);
		try{		
		List lista=new ArrayList();
		lista.addAll((List)query.execute());
		return lista;
		}catch(Exception ex){
			throw ex;
		}finally{
			query.closeAll();
		}
		
	}
}
