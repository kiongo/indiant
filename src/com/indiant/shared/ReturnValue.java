package com.indiant.shared;

import java.io.Serializable;

import com.indiant.beans.Pais;

public class ReturnValue implements Serializable{
	
	private String nameClass;
	private Object valueReturn;
	private Pais beanPais;
	public String getNameClass() {
		return nameClass;
	}
	public void setNameClass(String nameClass) {
		this.nameClass = nameClass;
	}
	public Object getValueReturn() {
		return valueReturn;
	}
	public void setValueReturn(Object valueReturn) {
		this.valueReturn = valueReturn;
	}
	public Pais getBeanPais() {
		return beanPais;
	}
	public void setBeanPais(Pais beanPais) {
		this.beanPais = beanPais;
	}
	
	
	
}
