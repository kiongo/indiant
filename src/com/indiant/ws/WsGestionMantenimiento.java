package com.indiant.ws;

import java.util.List;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.indiant.beans.Pais;
import com.indiant.process.GestionMantenimiento;
import com.indiant.shared.ReturnValue;

@Api(name="gestionMantenimiento",namespace=@ApiNamespace(ownerDomain="indiant.com",ownerName="indiant.com",packagePath="ws"))
public class WsGestionMantenimiento {
	
	@ApiMethod(name="insertarPais",path="insertarPais")
	public ReturnValue insertPais(Pais beanPais){
		ReturnValue returnValue=new ReturnValue();
		try{
			returnValue.setNameClass(Boolean.class.getSimpleName());
			returnValue.setValueReturn(GestionMantenimiento.insertarPais(beanPais));
		}catch(Exception ex){
			returnValue.setNameClass(Exception.class.getSimpleName());
			returnValue.setValueReturn(ex.getLocalizedMessage());
		}
		return returnValue;
	}
	
	@ApiMethod(name="listaPais",path="listaPais")
	public List<Pais> listaPais() throws Exception{		
		return GestionMantenimiento.getListPais();
	}
}
